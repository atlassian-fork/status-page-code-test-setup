from test_app import gen_app, celery, DevelopmentConfig

app = gen_app(DevelopmentConfig())
app.app_context().push()
